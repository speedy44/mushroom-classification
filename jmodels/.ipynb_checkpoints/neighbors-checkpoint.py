import math
import operator
class KNeighborsClassifier:
    def __init__(self,n_neighs = 5):
        self.n_neighs = n_neighs
        
    def calculate_distance(self,instance1, instance2, length):
        distance = 0
        for x in range(length):
            distance += pow(instance1[x] - instance2[x],2)
        return math.sqrt(distance)  
    
    def fit(self,X_train,y_train):
        self.X_train = X_train
        self.y_train = y_train
        
    def predict(self,X_test):
        distance = []
        index = 0
        length = len(X_test) 
        for x in range(len(self.X_train)):
            dist = self.calculate_distance(X_test,self.X_train[x],length)
            distance.append((index,dist))
            index += 1
        distance.sort(key = operator.itemgetter(1))
        neighbours = []
        for x in range(self.n_neighs):
            neighbours.append(self.y_train[distance[x][0]])
    #create a dictionarry that will keep count of the most frequent class   
        classvotes = {}
        for x in range(len(neighbours)):
            response = neighbours[x]
            if response in classvotes:
                classvotes[response] += 1
            else:
                classvotes[response] = 1
        sortedVotes=sorted(classvotes.items(),key= operator.itemgetter(1),reverse=True)
        return sortedVotes[0][0]
        