import numpy as np
class GaussianNB:
    def __init__(self):
        self.class_dict = {}
    
    def split_classes(self,class_dict,X_train,y_train):
        first_one = True
        first_zero = True
        for x in range(y_train.shape[0]):
            X_temp = X_train[x,:].reshape(X_train[x,:].shape[0],1)
            if y_train[x] == 0:
                if first_zero == True:
                    class_dict[0] = X_temp
                    first_zero = False
                else:
                    class_dict[0] = np.append(class_dict[0],X_temp,1)       
            elif y_train[x] == 1:
                if first_one == True:
                    class_dict[1] = X_temp
                    first_one = False
                else:
                    class_dict[1] = np.append(class_dict[1],X_temp,1)   
        return class_dict
      
    def fit(self,X_train,y_train):
        self.X_train = X_train
        self.y_train = y_train
        self.class_dict[0] = np.array([[]])
        self.class_dict[1] = np.array([[]])
        self.class_dict = self.split_classes(self.class_dict,self.X_train,self.y_train)
        self.mean_0 = np.mean(self.class_dict[0],axis = 1)
        self.mean_1 = np.mean(self.class_dict[1], axis = 1)
        self.std_0 = np.std(self.class_dict[0], axis = 1)
        self.std_1 = np.std(self.class_dict[1], axis = 1)
    
    
    def likelyhood(self,x,mean_,sigma):
#     don't mind the names
        numm = np.square(np.subtract(x, mean_))
        dt = np.multiply(np.square(sigma), 2)
        exp_power = -np.divide(numm, dt)    
        sig = np.divide(1, np.multiply(sigma, np.sqrt(2 * np.pi)))
        return (np.multiply(sig, np.exp(exp_power)))
    
    def posteriorProba(self,X,X_train_class,mean_,std_):
        evidence = self.likelyhood(X,mean_,std_)
        if len(evidence.shape) > 1:
            product = np.prod(evidence,axis=1)
        else:
            product = np.prod(evidence)
        return np.multiply(product, X_train_class.shape[0] / X.shape[0])
    
    def predict(self,X):
        p_class1 = self.posteriorProba(X,self.class_dict[1],self.mean_1,self.std_1)
        p_class0 = self.posteriorProba(X,self.class_dict[0],self.mean_0,self.std_0)
    #     The class with highest probability will be assigned to the test value
        probabilities = np.array([p_class0, p_class1])
        predicted_values = np.argmax(probabilities,axis = 0)
        return predicted_values
    
    def accuracy(self,x,y):
        y_pred = self.predict(x)
        accuracyy = np.mean(y_pred == y)
        return accuracyy*100
    