import numpy as np
import scipy.optimize as op
import pandas as pd


# A sigmoid function
def sigmoid(z):
    return 1/(1 + np.exp(-z))

class LogisticRegression:
    
    def __init__(self):
        pass
    
    #   A cost function  
    def logLoss(self,theta,X,y):
        m,n = X.shape
        theta = theta.reshape((n,1))
        y = y.reshape((m,1))
        term1 = np.log(sigmoid(X.dot(theta)))
        term2 = np.log(1 - sigmoid(X.dot(theta)))
        term1 = term1.reshape((m,1))
        term2 = term2.reshape((m,1))
        term = y * term1 + (1 - y) * term2;
        J = -((np.sum(term))/m);
        return J;

    # A gradient function
    def gradient(self,theta,X,y):
        m, n = X.shape
        theta = theta.reshape((n,1)).astype('float64')
        y = y.reshape((m,1))
        sigm_X = sigmoid(X.dot(theta))
        return ((np.dot(X.transpose(), sigm_X - y))/m).flatten()
    
    def fit(self,X,y):
        X = np.c_[np.ones((X.shape[0], 1)), X]
        m,n = X.shape
        theta = np.zeros(n)
        
        # Finding the best values of theta that can reduce the cost function
        result = op.minimize(fun = self.logLoss,x0 = theta
                         ,args = (X,y),method = 'BFGS',jac = self.gradient)
        self.weights = result.x
        return self
    
    def predictProba(self,X):
        theta = self.weights
        X = np.c_[np.ones((X.shape[0], 1)), X]
        return sigmoid(X.dot(theta))
    
    def predict(self,X,threshhold = 0.5):
        return np.array(list(map(lambda p: int(p), np.array(self.predictProba(X) >= threshhold))))
    
    def accuracy(self,x,y,threshhold = 0.5):
        y_pred = self.predict(x)
#         y_pred = y_pred.flatten()
        accuracyy = np.mean(y_pred == y)
        return accuracyy*100
